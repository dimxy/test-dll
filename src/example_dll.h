#ifndef EXAMPLE_DLL_H
#define EXAMPLE_DLL_H

#ifdef __cplusplus
extern "C" {
#endif


#if defined(_WIN32)
// Mingw
#ifdef BUILDING_EXAMPLE_DLL
#define EXAMPLE_DLL __declspec(dllexport)
#else
//#define EXAMPLE_DLL __declspec(dllimport)
#define EXAMPLE_DLL  
#endif

#elif defined(__GNUC__) && defined(BUILDING_EXAMPLE_DLL)
// Linux
#define EXAMPLE_DLL __attribute__((visibility("default")))
#else
#define EXAMPLE_DLL   
#endif

EXAMPLE_DLL void hello(const char *s);
void hello_static(const char *s);

int EXAMPLE_DLL  Double(int x);

#ifdef __cplusplus
}
#endif

// NOTE: this function is not declared extern "C"
void EXAMPLE_DLL CppFunc(void);


#endif  // EXAMPLE_DLL_H
